package main

import (
	"net/http"
	"sort"

	"github.com/labstack/echo/v5"
	"github.com/pocketbase/pocketbase/apis"
	"github.com/pocketbase/pocketbase/core"
)

type Meta struct {
	Description string
	Tags        []Tag
	Link_amount int
}

func implementMeta() {
	app.OnBeforeServe().Add(func(e *core.ServeEvent) error {
		e.Router.AddRoute(echo.Route{
			Method: http.MethodGet,
			Path:   "/meta",
			Handler: func(c echo.Context) error {

				meta := Meta{
					Description: config.Instance.Description,
				}

				// Fetch the amount of unique links
				var linkAmount int
				app.DB().
					Select("count(*)").
					From("links").
					Row(&linkAmount)
				meta.Link_amount = linkAmount

				var rawTags []Rawtag
				err := app.DB().
					Select("*").
					From("tags").
					All(&rawTags)
				if err != nil {
					return apis.NewNotFoundError("Error while fetching tags: ", err.Error())
				}

				// Construct list of tags from rawtags
				var tags []Tag
				for i := 0; i < len(rawTags); i++ {
					tags = append(tags, ConstructTag(rawTags[i]))
				}

				sort.Slice(tags, func(i1, i2 int) bool {
					return tags[i1].Link_amount > tags[i2].Link_amount
				})

				meta.Tags = tags

				return c.JSON(http.StatusOK, meta)
			},
			Middlewares: []echo.MiddlewareFunc{
				apis.ActivityLogger(app),
			},
		})

		return nil
	})
}
