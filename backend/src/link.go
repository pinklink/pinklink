package main

import (
	"log"
	"net/http"
	"strings"

	"github.com/labstack/echo/v5"
	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/apis"
	"github.com/pocketbase/pocketbase/core"
	"github.com/pocketbase/pocketbase/forms"
	"github.com/pocketbase/pocketbase/models"
)

type NewLink struct {
	Url                 string
	Name                string
	Description         string
	Curator_description string
	Tags                []string
}

type LinkToDelete struct {
	Url string
}

func implementLink() {
	app.OnBeforeServe().Add(func(e *core.ServeEvent) error {

		e.Router.AddRoute(echo.Route{
			Method: http.MethodPost,
			Path:   "/link",
			Handler: func(c echo.Context) error {
				var NewLink NewLink
				c.Bind(&NewLink)

				links, err := app.Dao().FindCollectionByNameOrId("links")
				if err != nil {
					return err
				}
				record := models.NewRecord(links)

				form := forms.NewRecordUpsert(app, record)

				form.LoadData(map[string]any{
					"url":                 NewLink.Url,
					"name":                NewLink.Name,
					"description":         NewLink.Description,
					"curator_description": NewLink.Curator_description,
					"tags":                NewLink.Tags,
				})

				// validate and submit (internally it calls app.Dao().SaveRecord(record) in a transaction)
				if err := form.Submit(); err != nil {
					return err
				}

				return c.NoContent(http.StatusOK)
			},
			Middlewares: []echo.MiddlewareFunc{
				apis.ActivityLogger(app),
				apis.RequireAdminAuth(),
			},
		})

		e.Router.AddRoute(echo.Route{
			Method: http.MethodGet,
			Path:   "/link/:url",
			Handler: func(c echo.Context) error {
				var url = c.PathParam("url")

				var rawLink Rawlink

				// Build an expression where it doesn't matter if the url in DB has a /-suffix or not
				var expr dbx.Expression
				if strings.HasSuffix(url, "/") {
					expr = dbx.Or(
						dbx.HashExp{"url": url},
						dbx.HashExp{"url": strings.TrimSuffix(url, "/")},
					)
				} else {
					expr = dbx.Or(
						dbx.HashExp{"url": url},
						dbx.HashExp{"url": url + "/"},
					)
				}

				err := app.DB().
					Select("*").
					From("links").
					Where(expr).
					One(&rawLink)
				if err != nil {
					log.Println(url, err.Error())
					return apis.NewNotFoundError("Link not found", err.Error())
				}

				var link = ConstructLink(rawLink)
				return c.JSON(http.StatusOK, link)
			},
			Middlewares: []echo.MiddlewareFunc{
				apis.ActivityLogger(app),
			},
		})

		e.Router.AddRoute(echo.Route{
			Method: http.MethodDelete,
			Path:   "/link",
			Handler: func(c echo.Context) error {
				var url LinkToDelete
				c.Bind(&url)
				record, err := app.Dao().FindFirstRecordByData("links", "url", url.Url)
				if err != nil {
					return apis.NewNotFoundError("Link not found", err.Error())
				}
				if err := app.Dao().DeleteRecord(record); err != nil {
					return err
				}

				return c.NoContent(http.StatusOK)
			},
			Middlewares: []echo.MiddlewareFunc{
				apis.ActivityLogger(app),
				apis.RequireAdminAuth(),
			},
		})

		return nil
	})
}
