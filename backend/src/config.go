package main

import (
	"log"
	"os"

	"github.com/pelletier/go-toml"
)

type Config struct {
	Instance Instance
}
type Instance struct {
	Url         string
	Description string
}

var config Config

func readConfig() {
	configFile, err := os.ReadFile("pb_data/config.toml")
	if err != nil {
		log.Fatalln("Error reading config.toml:", err)
	}

	err = toml.Unmarshal(configFile, &config)
	if err != nil {
		log.Fatalln("Error reading config.toml:", err)
	}
}
