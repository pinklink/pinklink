// main.go
package main

import (
	"log"

	"github.com/pocketbase/pocketbase"
)

var app *pocketbase.PocketBase

func main() {
	app = pocketbase.New()

	firstLaunchSetup()

	readConfig()

	// /search
	implementSearch()
	// /meta
	implementMeta()
	// /link
	implementLink()

	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}
