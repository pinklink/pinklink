package main

import (
	"fmt"
	"io/fs"
	"log"
	"os"

	"github.com/pocketbase/pocketbase/models"
	"github.com/pocketbase/pocketbase/models/schema"
	"github.com/spf13/cobra"
)

// Setup the db and config files
func firstLaunchSetup() {
	app.RootCmd.AddCommand(&cobra.Command{
		Use: "setup",
		Run: func(command *cobra.Command, args []string) {

			// Informed consent
			log.Println("Are you sure you want to setup pinklink? If you already have an installation in this folder, this will wipe all of it's data. Type 'y' to continue.")
			var input string
			fmt.Scanln(&input)
			if input != "y" {
				log.Fatal("Quitting..")
			}

			// Create default config file
			defaultConfig :=
				`[instance]
	url = "http://instanceurl.com/"
	description = "Brief description of the instance."`

			err := os.WriteFile("pb_data/config.toml", []byte(defaultConfig), fs.FileMode(0660))
			if err != nil {
				log.Fatal(err)
			}

			// Prompt admin account creation.
			log.Println("In order to automatically setup the database, you need to create an admin account using the instructions in the README. *Once you have created an account*, type 'y' to continue.")
			fmt.Scanln(&input)
			if input != "y" {
				log.Fatal("Quitting..")
			}

			// Delete existing db collections for good measure
			baseCollections, err := app.Dao().FindCollectionsByType(models.CollectionTypeBase)
			if err != nil {
				log.Fatal(err)
			}
			authCollections, err := app.Dao().FindCollectionsByType(models.CollectionTypeAuth)
			if err != nil {
				log.Fatal(err)
			}
			for i := 0; i < len(baseCollections); i++ {
				if err := app.Dao().DeleteCollection(baseCollections[i]); err != nil {
					log.Fatal(err)
				}
			}
			for i := 0; i < len(authCollections); i++ {
				if err := app.Dao().DeleteCollection(authCollections[i]); err != nil {
					log.Fatal(err)
				}
			}

			// Initialize db collections
			linksCollection := &models.Collection{
				Name:       "links",
				Type:       models.CollectionTypeBase,
				ListRule:   nil,
				ViewRule:   nil,
				CreateRule: nil,
				UpdateRule: nil,
				DeleteRule: nil,
				Schema: schema.NewSchema(
					&schema.SchemaField{
						Name:     "url",
						Type:     schema.FieldTypeUrl,
						Required: true,
						Unique:   true,
					},
					&schema.SchemaField{
						Name:     "name",
						Type:     schema.FieldTypeText,
						Required: true,
						Unique:   false,
					},
					&schema.SchemaField{
						Name:     "description",
						Type:     schema.FieldTypeText,
						Required: false,
						Unique:   false,
					},
					&schema.SchemaField{
						Name:     "curator_description",
						Type:     schema.FieldTypeText,
						Required: false,
						Unique:   false,
					},
					&schema.SchemaField{
						Name:     "tags",
						Type:     schema.FieldTypeRelation,
						Required: false,
						Unique:   false,
						Options: &schema.RelationOptions{
							CollectionId:  "tagscollection",
							CascadeDelete: false,
						},
					},
				),
			}
			if err := app.Dao().SaveCollection(linksCollection); err != nil {
				log.Fatal(err)
			}

			tagsCollection := &models.Collection{
				Name:       "tags",
				Type:       models.CollectionTypeBase,
				ListRule:   nil,
				ViewRule:   nil,
				CreateRule: nil,
				UpdateRule: nil,
				DeleteRule: nil,
				Schema: schema.NewSchema(
					&schema.SchemaField{
						Name:     "name",
						Type:     schema.FieldTypeText,
						Required: true,
						Unique:   true,
					},
					&schema.SchemaField{
						Name:     "synonyms",
						Type:     schema.FieldTypeText,
						Required: false,
						Unique:   false,
					},
				),
			}
			tagsCollection.SetId("tagscollection")
			tagsCollection.MarkAsNew()
			if err := app.Dao().SaveCollection(tagsCollection); err != nil {
				log.Fatal(err)
			}

			log.Print("Setup done!")

		},
	})
}
