package main

import (
	"database/sql"
	"net/http"
	"strings"

	"github.com/labstack/echo/v5"
	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase/apis"
	"github.com/pocketbase/pocketbase/core"
)

func implementSearch() {
	app.OnBeforeServe().Add(func(e *core.ServeEvent) error {
		e.Router.AddRoute(echo.Route{
			Method: http.MethodGet,
			Path:   "/search",
			Handler: func(c echo.Context) error {

				var result []Rawlink

				query := c.QueryParam("q")

				// Convert the comma-delimited tags querystring into a list of tagIDs to filter by
				tagsString := c.QueryParam("tags")
				var tagIDs []string
				if len(tagsString) > 0 {
					tagNames := strings.Split(tagsString, ",")
					var tags []Rawtag
					for i := 0; i < len(tagNames); i++ {
						var tag Rawtag
						err := app.DB().
							Select("*").
							From("tags").
							Where(dbx.Or(
								dbx.Like("name", tagNames[i]),
								dbx.Like("synonyms", tagNames[i]),
							)).
							One(&tag)
						if err != nil {
							// If there are no tags with the specified names, just return a list of 0 links (nil)
							if err == sql.ErrNoRows {
								return c.JSON(http.StatusOK, nil)
							}
							return apis.NewNotFoundError("Error while fetching tags: ", err.Error())
						}
						tags = append(tags, tag)
					}
					tagIDs = append(tagIDs, "")
					for i := 0; i < len(tags); i++ {
						tagIDs = append(tagIDs, tags[i].Id)
					}
				}

				err := app.DB().
					Select("*").
					From("links").
					Where(dbx.And(
						dbx.Or(
							dbx.Like("url", query),
							dbx.Like("name", query),
							dbx.Like("description", query),
							dbx.Like("curator_description", query),
						),
						dbx.Like("tags", tagIDs...),
					)).
					All(&result)

				if err != nil {
					return apis.NewNotFoundError("Error while searching: ", err.Error())
				}

				var links []Link
				for i := 0; i < len(result); i++ {
					rawlink := result[i]
					links = append(links, ConstructLink(rawlink))
				}

				return c.JSON(http.StatusOK, links)
			},
			Middlewares: []echo.MiddlewareFunc{
				apis.ActivityLogger(app),
			},
		})

		return nil
	})
}
