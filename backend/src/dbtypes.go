package main

import (
	"encoding/json"
	"log"
	"strings"

	"github.com/pocketbase/dbx"
)

type Link struct {
	Url                 string
	Name                string
	Description         string
	Curator_description string
	Instance_url        string
	Tags                []Tag
}

type Rawlink struct {
	Url                 string
	Name                string
	Description         string
	Curator_description string
	Tags                string
}

// Handles expansion of tag ids from the db and adds the right instance_url
func ConstructLink(raw Rawlink) Link {
	link := Link{
		Url:                 raw.Url,
		Name:                raw.Name,
		Description:         raw.Description,
		Curator_description: raw.Curator_description,
		Instance_url:        config.Instance.Url,
	}

	var rawTagIds []string
	json.Unmarshal([]byte(raw.Tags), &rawTagIds)

	var tags []Tag
	for i := 0; i < len(rawTagIds); i++ {
		ID := rawTagIds[i]
		record, err := app.Dao().FindRecordById("tags", ID)
		if err != nil {
			log.Println(err.Error())
			return link
		}
		tags = append(tags, Tag{
			Id:          record.Id,
			Name:        record.GetString("name"),
			Synonyms:    strings.Split(record.GetString("synonyms"), ","),
			Link_amount: getTagLinkAmount(ID),
		})
	}

	link.Tags = tags

	return link
}

type Rawtag struct {
	Id          string
	Name        string
	Synonyms    string
	Link_amount int
}

type Tag struct {
	Id          string
	Name        string
	Synonyms    []string
	Link_amount int
}

// Handles unmarshaling of synonyms from the db
func ConstructTag(raw Rawtag) Tag {
	tag := Tag{
		Id:          raw.Id,
		Name:        raw.Name,
		Link_amount: getTagLinkAmount(raw.Id),
	}

	Synonyms := strings.Split(raw.Synonyms, ",")
	tag.Synonyms = Synonyms

	return tag
}

func getTagLinkAmount(tagID string) int {
	var linkAmount int
	app.DB().
		Select("count(*)").
		From("links").
		Where(dbx.Like("tags", tagID)).
		Row(&linkAmount)
	return linkAmount
}
