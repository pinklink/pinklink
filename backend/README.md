# ***pinkl.ink (pinklink) is***
* a small protocol for the federation of link directories
* a piece of software you can use to store your bookmarks and share them with the world
* an objectively cool way to find new websites

### ***inspiration***
* https://links.yesterweb.org/
* https://pinboard.in/

---

### ***this folder contains the pinkl.ink reference backend implementation using [Pocketbase](https://pocketbase.io/)***  
* for the backend api "protocol" / documentation: [click here](docs/README.md)

---

### ***setup***   
In order to manually run only this backend, follow these steps.  
1. Download a binary (not available yet) or build it yourself using the build instructions below.
2. Pick a folder for the data and configuration to be stored in. You will then have to run pinklink out of that folder in order for it to have access to your data. The user account that pinklink is ran under has to have proper read/write access to the directory.
3. Open the terminal, navigate to the folder you picked and run the command `./pinklink-backend setup`
4. When the setup command prompts you to create an admin account, open another terminal with the same directory open, run `./pinklink-backend serve`. This should start the api webserver and the Pocketbase admin page.
5. Go to the Pocketbase admin page (by default http://localhost:8090/_/, make sure to replace *localhost* with the ip-address of your webserver if needed).
6. Once on the admin page, create your admin account. Make sure to remember the credentials, since they are the same ones that you can use to edit your link collection.

### ***running the backend***   
Once you have completed the setup, you can run the backend with `./pinklink-backend serve`. This is a stock command of Pocketbase, and you can set the http port and enable https using the flags `--http` and `-https`, for example   
`./pinklink-backend serve --http="yourdomain.com:80" --https="yourdomain.com:443"`.   
Note that you need to have permission to bind to the ports you select. A reverse proxy is not needed to run the backend with https since Pocketbase automatically sets up a Let's Encrypt certificate.

### ***building***
The build command used can be found inside `build.sh`, and it should produce a working executable when run on your machine. Building has been tested on `go version go1.19.2 linux/amd64`. 