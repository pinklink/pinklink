### ***structs***

#### link:
* Url
* Name
* Tags
	* list of the tags attached to the link
* Description
	* the description of the link as supplied by the website hosted at the url
* Curator_description
	* a description of the link as supplied by the curator of the link *optional, preferred over description in ui*
* Instance_url [on-demand, not in db]
	* the url of the instance of pinklink

#### tag:
* Name
* Synonyms
	* list of synonyms of the tag; when filtering by this tag also filter by the synonyms
	* you should use only one tag per topic on your instance, but this is useful if another instance uses a different tag name for the same topic
* Link_amount [on-demand, not in db]
	* amount of unique links with the tag