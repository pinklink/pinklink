### ***api***

/search *(get)*:
* params
	* ?q=
		* search query for metadata / descriptions
	* ?tags=
		* list of capitalization-agnostic tags to filter by
	* ?federate=true/false
		* specify whether to search federated or locally
	* ?page= [might not be needed]
		* number of the result page, starting from zero
* results
	* json array of links

/meta *(get)*:
* results
  * json object including
  	* Description
  		* a short description of the instance, who runs it, how's it curated
  	* [Tags]
  		* a list of all tags used on the site
  	* Link_amount
  		* amount of unique links the instance has
  
/link *(post)*:
* body
  * json object including
    * Url
    * Name
    * Tags
    	* list of the tag id's of tags describing the link
    * Description
    	* the description of the link as supplied by the website hosted at the url
    * Curator_description
    	* a description of the link as supplied by the curator of the link *optional, preferred over description in ui*
* results
  * http statuscode corresponding to request status

/link/[url] *(get)*:
* results
  * single link struct or error if doesn't exist

/link *(delete)*:
* body
  * json object including
    * Url
* results
  * http statuscode corresponding to request status