## TODO

* Search queries should also match with tags
* Pagination (can't always return all resuls at once)
* Federated search
* Date added/modified for links

## DOING

## DONE

* `/link` to get single link
* Config file
* `/search?q=`
* `/search?tags=`
* `/meta`
* first launch setup
* `/link`