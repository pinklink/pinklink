# ***pinkl.ink (pinklink) is***
* a small protocol for the federation of link directories
* a piece of software you can use to store your bookmarks and share them with the world
* an objectively cool way to find new websites

### ***inspiration***
* https://links.yesterweb.org/
* https://pinboard.in/

---

### ***this folder contains the pinkl.ink reference frontend implementation using [SvelteKit](https://kit.svelte.dev/)***

---

### ***run / deploy***
In order to manually run only this frontend **for development**, follow these steps.  
1. If you intend on contributing any changes to the code, fork this repository
2. Clone the forked repository
3. Open terminal in this folder
4. Run `npm install` (you must have `npm` and a newish version of `node` installed)
5. Run `npm run dev` to run the project in development mode

And in order to manually **deploy** only this frontend, follow these steps.  
1. If you intend on making any changes to the code, fork this repository
2. Choose where and how to deploy. SvelteKit can be deployed to numerous hosting providers and Node.js
3. Follow the guides made by your hosting provider and the [official docs from Svelte](https://kit.svelte.dev/docs/adapters)
4. Before deploying remember to fill in the required configuration options in the `.env` file

---

### ***attribution***
The "info" icon is a part of the icon set CoreUI Icons and licensed under CC BY 4.0.