import { PUBLIC_POCKETBASE_URL } from '$env/static/public';

/** @type {import('./$types').PageLoad} */
export async function load({ url, fetch }) {
	let query = url.searchParams.get('q');
	let tags = url.searchParams.get('tags');

	// When the page is first loaded, the queryparams are null, but searching with them isn't proper functioning
	if (query == null) {
		query = "";
	}
	if (tags == null) {
		tags = "";
	}

	tags = tags.slice(0, -1);

	const linksUrl = `${PUBLIC_POCKETBASE_URL}/search?q=${query}&tags=${tags}`;
	const linksres = await fetch(linksUrl);
	const links = linksres.json();

	const metares = await fetch(`${PUBLIC_POCKETBASE_URL}/meta`);
	const meta = metares.json();

	return {
	  links: links,
	  meta: meta,
	};
  }