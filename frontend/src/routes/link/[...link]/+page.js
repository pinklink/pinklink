import { PUBLIC_POCKETBASE_URL } from '$env/static/public';

/** @type {import('./$types').PageLoad} */
export async function load({ params, fetch }) {
	let url = params.link;

	console.log(url)

	const linkUrl = `${PUBLIC_POCKETBASE_URL}/link/${url}`;
	const linkres = await fetch(linkUrl);
	const link = linkres.json();

	return {
	  link: link,
	};
  }