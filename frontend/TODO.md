## TODO

* Tag "info" button
* Tags hide with `<details>`, show x amount always, show more with a click
* Auth
* Bookmark info/add page `/link/https://`
* Federated search

## DOING

## DONE

* Fix the Vite+Svelte environment (or start over)
* Instance info
* Tags
* Search
* Choose framework (Sveltekit)