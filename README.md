note: development halted

# ***pinkl.ink (pinklink)***

*the internet is full of curated directories, why not have them all in one place*

* a small protocol for the federation of link directories
* a piece of software you can use to store your bookmarks and share them with the world
* an objectively cool way to find new websites

### ***inspiration***
* https://links.yesterweb.org/
* https://pinboard.in/

---

### ***this is the pinklink monorepo***  

this repo contains both the reference backend and frontend implementations and a container image to run both is planned.

this is **unfinished software** and the development of both the refence backend and frontend is very *work-in-progress*

---

### ***implementation plan***

requirements:
* lightweight
* portable

elements & tools:
* full backend (database, auth, api)
	* https://pocketbase.io/
		* https://echo.labstack.com/ and sqlite under the hood
* frontend
	* sveltekit

deployment:
* single-binary backend
* container for frontend & combined

---

### ***integrations***

raindrop.io:
https://developer.raindrop.io/

---

